# Virtual Pricing Director Technical Assessment


# Prerequisites
 - All code must be written in Typescript


## Tasks
Using the existing typescript template in the main branch, add an API that consists of 3 endpoints
1. Create a user, (Add a new record to the ./mock-data/user.json file)
2. Return the active users from the ./mock-data/user.json file, with pagination filtering by email.
3. Create a bulk entry of time entries for a given user and expose an endpoint to retrieve time entries for a given user with pagination.

4. Write a short paragraph of how you would improve your solution if you had more time.

> _Write Paragraph Here_

### Evaluation Criteria

-   **TypeScript** best practices
-   We're looking for you to produce working code, with enough room to demonstrate how to create structure in a small program.
-   Completeness: did you complete the features?
-   Correctness: does the functionality act in sensible, thought-out ways?
-   Scalability: Can the application be scaled with large datasets and high request counts?
-   Maintainability: is it written in a clean, maintainable way?
-   Testing: is the system adequately tested?

# Submission
Please organize, design, test and document your code as if it were
going into production - then zip your solution and send file transfer of your choice.

All the best.

The Virtual Pricing Director Team
